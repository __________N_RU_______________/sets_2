#ifndef SETS_2_LISTSET_H
#define SETS_2_LISTSET_H

#include <string>
#include <iostream>

using namespace std;

class ListSet {
private:
	/** Минимальный элемент в последовательности символов */
	const static char MIN_SET_ELEMENT = '0';

	/** Максимальный элемент в последовательности символов */
	const static char MAX_SET_ELEMENT = '9';

	/** Мощность универсума */
	const static int UNIVERSE_CARDINALITY = 10;

	/** Счетчик множеств */
	static int counter;

	/** Мощность множества */
	int cardinality;

	/** Тег множества */
	char tag;

	/** Класс элемента списка */
	class Node {
	private:
		char data;
		Node *next;
	public:
		Node(char data, Node *next = nullptr): data(data), next(next) {
			cout << "+" << data;
		};
		friend class ListSet;
	};

	/** Указатель на первый элемент списка */
	Node *set_storage = nullptr;

	/** Добавляет элемент в начало списка */
	inline void pushFront(char element) {
		set_storage = new Node(element, set_storage);
		cardinality++;
	};

	/** Получает элемент множества по позиции бита */
	static char getSetElementByPos(int);

	/** Получает позицию бита по элементу множества */
	static int getPosBySetElement(char);
public:
	/** Имя теста */
	static const std::string TEST_NAME;

	/** Объединение множеств */
	ListSet operator | (const ListSet&) const;

	/** Пересечение множеств */
	ListSet operator & (const ListSet&) const;

	/** Объединенная операция пересечения и присваивания */
	ListSet& operator &= (const ListSet&);

	/** Объединенная операция объединения и присваивания */
	ListSet& operator |= (const ListSet&);

	/** Дополнение до универсума */
	ListSet operator ~ () const;

	/** Оператор присваивания */
	ListSet& operator = (const ListSet&);

	/** Вывод множества */
	void show();

	/** Конструктор множества */
	ListSet(char, bool isRandomlyFilled = true);

	/** Базовый конструктор множества */
	ListSet();

	/** Конструктор копии множества */
	ListSet(const ListSet&);
	ListSet(ListSet &&);

	~ListSet() {
		cout << "Удалено " << tag << "(" << cardinality << ")" << endl;
		delete set_storage;
	}
};

#endif //SETS_2_LISTSET_H
