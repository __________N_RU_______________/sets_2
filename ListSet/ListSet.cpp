#include <iostream>
#include <string>

#include "ListSet.h"

using namespace std;

int ListSet::counter = 0;
const string ListSet::TEST_NAME = "Тест множеств на списках";

ListSet::ListSet(): tag('0'), cardinality(0) {
	counter++;
}

ListSet::ListSet(char tag, bool isRandomlyFilled): ListSet() {
	this->tag = tag;

	if (!isRandomlyFilled) {
		cout << "-> Создано " << tag << "(" << cardinality << ")" << endl;
		return;
	}

	for (int i = 0; i < UNIVERSE_CARDINALITY; i++) {
		if (rand() % 2) {
			auto element = ListSet::getSetElementByPos(i);
			pushFront(element);
		}
	}

	cout << "-> Создано " << tag << "(" << cardinality << ")" << endl;
}

ListSet::ListSet(ListSet &&B): ListSet() {
	cout << "-> Принято " << tag << "(" << cardinality << ")";
	cout << " из " << B.tag << "(" << B.cardinality << ")" << endl;

	tag = B.tag;
	cardinality = B.cardinality;

	set_storage = B.set_storage;
	B.set_storage = nullptr;
}

ListSet::ListSet(const ListSet &B) {
	counter++;
	tag = B.tag;
	cardinality = B.cardinality;

	if (B.set_storage == nullptr) {
		set_storage = nullptr;
		return;
	}

	set_storage = new Node(B.set_storage->data, B.set_storage->next);

	cout << "-> Создано " << tag << "(" << cardinality << ")";
	cout << " из " << B.tag << "(" << B.cardinality << ")" << endl;
}

ListSet& ListSet::operator |= (const ListSet &B) {
	for (auto pB = B.set_storage; pB; pB = pB->next) {
		bool hasElement = false;

		for (auto p = set_storage; p; p = p->next) {
			if (pB->data == p->data) {
				hasElement = true;
			}
		}


		if (!hasElement) {
			pushFront(pB->data);
		}
	}

	cout << "-> Получено " << tag << "(" << cardinality << ") = ";
	cout << tag << " | " << B.tag << endl;

	return *this;
}

ListSet ListSet::operator | (const ListSet &B) const {
	auto C(*this);
	cout << "-> Вычисление " << tag << " | " << B.tag << endl;
	return (C |= B);
}

ListSet& ListSet::operator &= (const ListSet &B) {
	auto C(*this);
	cardinality = 0;
	set_storage = nullptr;

	for (auto pB = B.set_storage; pB; pB = pB->next) {
		for (auto p = C.set_storage; p; p = p->next) {
			auto isEqual = pB->data == p->data;

			if (isEqual) {
				pushFront(pB->data);
			}
		}
	}

	cout << "-> Получено " << tag << "(" << cardinality << ") = ";
	cout << C.tag << " & " << B.tag << endl;

	return *this;
}

ListSet ListSet::operator & (const ListSet &B) const {
	auto C(*this);
	cout << "-> Вычисление " << tag << " & " << B.tag << endl;
	return (C &= B);
}

ListSet ListSet::operator ~ () const {
	ListSet C;

	for (auto uElem = MIN_SET_ELEMENT; uElem < MAX_SET_ELEMENT; uElem++) {
		bool hasElement = false;

		for (auto p = set_storage; p; p = p->next) {
			if (p->data == uElem) {
				hasElement = true;
			}
		}

		if (!hasElement) {
			C.pushFront(uElem);
			C.cardinality++;
		}
	}

	cout << "-> Получено " << C.tag << "(" << C.cardinality << ") = ";
	cout << " = ~" << tag << endl;

	return C;
}

ListSet& ListSet::operator = (const ListSet &B) {
	if (this == &B) {
		return *this;
	}

	cout << "-> Удалено " << tag << "(" << cardinality << ")" << endl;
	cout << "-> Создано " << B.tag << "(" << B.cardinality << ")" << endl;

	cardinality = B.cardinality;
	set_storage = new Node(B.set_storage->data, B.set_storage->next);

	return *this;
}

void ListSet::show() {
	cout << "Множество " << tag << " ";
	cout << "{";

	int cnt = 0;
	for (auto p = set_storage; p; p = p->next) {
		cout << (char) p->data;

		if (cnt + 1 < cardinality) {
			cout << ", ";
		}

		cnt++;
	}

	cout << "}";
	cout << endl;
}

int ListSet::getPosBySetElement(char element) {
	return (int) (element - MIN_SET_ELEMENT);
}

char ListSet::getSetElementByPos(int position) {
	return (char) (MIN_SET_ELEMENT + position);
}
