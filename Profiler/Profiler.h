#ifndef SETS_2_PROFILER_H
#define SETS_2_PROFILER_H

class Profiler {
private:
	/** Время начала профилирования */
	long beginTime;

	/** Время конца профилирования */
	long endTime;

	/** Время выполнения профилированной функции */
	long executionTime;
public:
	/** Запускает профилирование */
	void startProfile();

	/** Останавливает профилирование */
	void stopProfile();

	/** Возвращает время исполнения функции в секундах */
	float getExecutionTime();
};


#endif //SETS_2_PROFILER_H
