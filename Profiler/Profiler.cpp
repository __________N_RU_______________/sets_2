#include <iostream>
#include "Profiler.h"

void Profiler::startProfile() {
	beginTime = clock();
}

void Profiler::stopProfile() {
	endTime = clock();
	executionTime = endTime - beginTime;
}

float Profiler::getExecutionTime() {
	return (float) executionTime / CLOCKS_PER_SEC;
}