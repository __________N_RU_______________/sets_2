#include <iostream>
#include <string>

#include "WordSet.h"

using namespace std;

int WordSet::counter = 0;
const string WordSet::TEST_NAME = "Тест множеств на машинном слове";

WordSet::WordSet(): tag('0') {
	counter++;
}

WordSet::WordSet(char tag, bool isRandomlyFilled): WordSet() {
	tag = tag;

	if (!isRandomlyFilled) {
		return;
	}

	set_storage = rand();
}

WordSet& WordSet::operator |= (const WordSet &B) {
	set_storage = set_storage | B.set_storage;

	return *this;
}

WordSet WordSet::operator | (const WordSet &B) const {
	WordSet C(*this);
	return (C |= B);
}

WordSet& WordSet::operator &= (const WordSet &B) {
	set_storage = set_storage & B.set_storage;

	return *this;
}

WordSet WordSet::operator & (const WordSet &B) const {
	WordSet C(*this);
	return (C &= B);
}

WordSet WordSet::operator ~ () const {
	WordSet C(*this);
	C.set_storage = !set_storage;

	return C;
}

void WordSet::show() {
	auto charSet = new char[UNIVERSE_CARDINALITY + 1];

	int setCounter = 0;
	for (int i = 0; i < UNIVERSE_CARDINALITY; i++) {
		int isBitActive = (set_storage >> i) & 1;

		if (isBitActive) {
			charSet[setCounter] = getSetElementByPos(i);
			setCounter++;
		}
	}

	charSet[UNIVERSE_CARDINALITY] = '\0';

	cout << "Множество " << tag << " ";
	cout << "{";

	for (int i = 0; i < setCounter; i++) {
		cout << charSet[i];

		if (i + 1 < setCounter) {
			cout << ", ";
		}
	}

	cout << "}";
	cout << endl;

	delete []charSet;
}

char WordSet::getSetElementByPos(int position) {
	return (char) (MIN_SET_ELEMENT + position);
}
