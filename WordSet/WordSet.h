#ifndef SETS_2_WORDSET_H
#define SETS_2_WORDSET_H

#include <string>

class WordSet {
private:
	/** Минимальный элемент в последовательности символов */
	const static char MIN_SET_ELEMENT = '0';

	/** Максимальный элемент в последовательности символов */
	const static char MAX_SET_ELEMENT = '9';

	/** Мощность универсума */
	const static int UNIVERSE_CARDINALITY = 10;

	/** Счетчик множеств */
	static int counter;

	/** Тег множества */
	char tag;

	/** Память для множества */
	unsigned int set_storage = 0;

	/** Получает элемент множества по позиции бита */
	static char getSetElementByPos(int);
public:
	/** Имя теста */
	static const std::string TEST_NAME;

	/** Объединение множеств */
	WordSet operator | (const WordSet&) const;

	/** Пересечение множеств */
	WordSet operator & (const WordSet&) const;

	/** Объединенная операция пересечения и присваивания */
	WordSet& operator &= (const WordSet&);

	/** Объединенная операция объединения и присваивания */
	WordSet& operator |= (const WordSet&);

	/** Дополнение до универсума */
	WordSet operator ~ () const;

	/** Вывод множества */
	void show();

	/** Конструктор множества */
	WordSet(char, bool isRandomlyFilled = true);

	/** Базовый конструктор множества */
	WordSet();
};

#endif //SETS_2_WORDSET_H
