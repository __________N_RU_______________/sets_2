#ifndef SETS_2_SET_H
#define SETS_2_SET_H

#include <string>

class ArraySet {
private:
	/** Минимальный элемент в последовательности символов */
	const static char MIN_SET_ELEMENT = '0';

	/** Максимальный элемент в последовательности символов */
	const static char MAX_SET_ELEMENT = '9';

	/** Мощность универсума */
	const static int UNIVERSE_CARDINALITY = 10;

	/** Счетчик множеств */
	static int counter;

	/** Мощность множества */
	int cardinality;

	/** Тег множества */
	char tag;

	/** Память для множества */
	char *set_storage;

	/** Получает элемент множества по позиции бита */
	static char getSetElementByPos(int);

	/** Получает позицию бита по элементу множества */
	static int getPosBySetElement(char);
public:
	/** Имя теста */
	static const std::string TEST_NAME;

	/** Объединение множеств */
	ArraySet operator | (const ArraySet&) const;

	/** Пересечение множеств */
	ArraySet operator & (const ArraySet&) const;

	/** Объединенная операция пересечения и присваивания */
	ArraySet& operator &= (const ArraySet&);

	/** Объединенная операция объединения и присваивания */
	ArraySet& operator |= (const ArraySet&);

	/** Дополнение до универсума */
	ArraySet operator ~ () const;

	/** Оператор присваивания */
	ArraySet& operator = (const ArraySet&);

	/** Вывод множества */
	void show();

	/** Получение мощности множества */
	inline int power() {
		return cardinality;
	};

	/** Конструктор множества */
	ArraySet(char, bool isRandomlyFilled = true);

	/** Базовый конструктор множества */
	ArraySet();

	/** Конструктор копии множества */
	ArraySet(const ArraySet&);

	/** Конструктор копии с переносом */
	ArraySet(ArraySet&&);

	/** Деструктор множества */
	inline ~ArraySet() {
		delete []set_storage;
	}
};

#endif //SETS_2_SET_H
