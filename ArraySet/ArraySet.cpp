#include <iostream>
#include <string>

#include "ArraySet.h"

using namespace std;

int ArraySet::counter = 0;
const string ArraySet::TEST_NAME = "Тест множеств на массивах";

ArraySet::ArraySet(): tag('0'), cardinality(0), set_storage(new char[UNIVERSE_CARDINALITY + 1]) {
	counter++;
	set_storage[0] = 0;
}

ArraySet::ArraySet(char tag, bool isRandomlyFilled): ArraySet() {
	this->tag = tag;

	if (!isRandomlyFilled) {
		return;
	}

	for (int i = 0; i < UNIVERSE_CARDINALITY; i++) {
		if (rand() % 2) {
			set_storage[cardinality] = ArraySet::getSetElementByPos(i);
			cardinality++;
		}
	}

	set_storage[cardinality + 1] = 0;
}

ArraySet::ArraySet(const ArraySet &B): ArraySet() {
	tag = B.tag;
	cardinality = B.cardinality;

	char *dst(set_storage);
	char *src(B.set_storage);

	while (*dst++ = *src++);
}

ArraySet::ArraySet(ArraySet &&B): ArraySet() {
	tag = B.tag;
	cardinality = B.cardinality;

	set_storage = B.set_storage;
	B.set_storage = nullptr;
}

ArraySet& ArraySet::operator = (const ArraySet &B) {
	if (this == &B) {
		return *this;
	}


	char *dst(set_storage);
	char *src(B.set_storage);

	cardinality = B.cardinality;

	while (*dst++ = *src++);

	return *this;
}

ArraySet& ArraySet::operator &= (const ArraySet &B) {
	auto C = ArraySet(*this);
	cardinality = 0;

	for (int i = 0; i < C.cardinality; i++) {
		for (int j = 0; j < B.cardinality; j++) {
			bool isEqual = C.set_storage[i] == B.set_storage[j];

			if (isEqual) {
				set_storage[cardinality] = C.set_storage[i];
				cardinality++;
			}
		}
	}

	set_storage[cardinality] = 0;
	return *this;
}

ArraySet ArraySet::operator & (const ArraySet &B) const {
	auto C = ArraySet(*this);
	return (C &= B);
}

ArraySet& ArraySet::operator |= (const ArraySet &B) {
	for (int i = 0; i < B.cardinality; i++) {
		bool hasElement = false;

		for (int j = 0; j < cardinality; j++) {
			if (B.set_storage[i] == set_storage[j]) {
				hasElement = true;
				break;
			}
		}

		if (!hasElement) {
			set_storage[cardinality] = B.set_storage[i];
			cardinality++;
		}
	}

	set_storage[cardinality] = 0;
	return *this;
}

ArraySet ArraySet::operator | (const ArraySet &B) const {
	auto C = ArraySet(*this);
	return (C |= B);
}

ArraySet ArraySet::operator ~ () const {
	ArraySet C;

	for (char uElem = MIN_SET_ELEMENT; uElem < MAX_SET_ELEMENT; uElem++) {
		bool hasElement = false;

		for (int j = 0; j < cardinality; j++) {
			if (set_storage[j] == uElem) {
				hasElement = true;
				break;
			}
		}

		if (!hasElement) {
			C.set_storage[C.cardinality] = uElem;
			C.cardinality++;
		}
	}

	C.set_storage[C.cardinality] = 0;
	return C;
}

void ArraySet::show() {
	cout << "Множество " << tag << " ";
	cout << "{";

	for (int i = 0; i < cardinality; i++) {
		cout << (char) set_storage[i];

		if (i + 1 < cardinality) {
			cout << ", ";
		}
	}

	cout << "}";
	cout << endl;
}

int ArraySet::getPosBySetElement(char element) {
	return (int) (element - MIN_SET_ELEMENT);
}

char ArraySet::getSetElementByPos(int position) {
	return (char) (MIN_SET_ELEMENT + position);
}
