#include <iostream>
#include <string>

#include "BitsArraySet.h"

using namespace std;

int BitsArraySet::counter = 0;
const string BitsArraySet::TEST_NAME = "Тест множеств на массивах битов";

BitsArraySet::BitsArraySet(): tag('0'), set_storage(new int[UNIVERSE_CARDINALITY + 1]) {
	counter++;
	set_storage[0] = 0;
}

BitsArraySet::BitsArraySet(char tag, bool isRandomlyFilled): BitsArraySet() {
	this->tag = tag;

	if (!isRandomlyFilled) {
		return;
	}

	for (int i = 0; i < UNIVERSE_CARDINALITY; i++) {
		set_storage[i] = rand() % 2 ? 0 : 1;
	}
}

BitsArraySet& BitsArraySet::operator |= (const BitsArraySet &B) {
	for (int i = 0; i < UNIVERSE_CARDINALITY; i++) {
		set_storage[i] = B.set_storage[i] | set_storage[i];
	}

	return *this;
}

BitsArraySet BitsArraySet::operator | (const BitsArraySet &B) const {
	auto C = BitsArraySet(*this);
	auto D = (C |= B);
	return D;
}

BitsArraySet& BitsArraySet::operator &= (const BitsArraySet &B) {
	auto C = BitsArraySet(*this);

	for (int i = 0; i < UNIVERSE_CARDINALITY; i++) {
		set_storage[i] = B.set_storage[i] & C.set_storage[i];
	}

	return *this;
}

BitsArraySet BitsArraySet::operator & (const BitsArraySet &B) const {
	auto C = BitsArraySet(*this);
	return (C &= B);
}

BitsArraySet BitsArraySet::operator ~ () const {
	auto C = new BitsArraySet();

	for (int i = 0; i < UNIVERSE_CARDINALITY; i++) {
		C->set_storage[i] = !this->set_storage[i];
	}

	return *C;
}

void BitsArraySet::show() {
	auto charSet = new char[UNIVERSE_CARDINALITY + 1];

	int setCounter = 0;
	for (int i = 0; i < UNIVERSE_CARDINALITY; i++) {
		int isBitActive = set_storage[i] == 1;

		if (isBitActive) {
			charSet[setCounter] = getSetElementByPos(i);
			setCounter++;
		}
	}

	charSet[UNIVERSE_CARDINALITY] = '\0';

	cout << "Множество " << tag << " ";
	cout << "{";

	for (int i = 0; i < setCounter; i++) {
		cout << charSet[i];

		if (i + 1 < setCounter) {
			cout << ", ";
		}
	}

	cout << "}";
	cout << endl;

	delete []charSet;
}

int BitsArraySet::getPosBySetElement(char element) {
	return (int) (element - MIN_SET_ELEMENT);
}

char BitsArraySet::getSetElementByPos(int position) {
	return (char) (MIN_SET_ELEMENT + position);
}