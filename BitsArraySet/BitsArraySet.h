#ifndef SETS_2_BITSARRAYSET_H
#define SETS_2_BITSARRAYSET_H

#include <string>

class BitsArraySet {
private:
	/** Минимальный элемент в последовательности символов */
	const static char MIN_SET_ELEMENT = '0';

	/** Максимальный элемент в последовательности символов */
	const static char MAX_SET_ELEMENT = '9';

	/** Мощность универсума */
	const static int UNIVERSE_CARDINALITY = 10;

	/** Счетчик множеств */
	static int counter;

	/** Тег множества */
	char tag;

	/** Память для множества */
	int *set_storage;

	/** Получает элемент множества по позиции бита */
	static char getSetElementByPos(int);

	/** Получает позицию бита по элементу множества */
	static int getPosBySetElement(char);
public:
	/** Имя теста */
	static const std::string TEST_NAME;

	/** Объединение множеств */
	BitsArraySet operator | (const BitsArraySet&) const;

	/** Пересечение множеств */
	BitsArraySet operator & (const BitsArraySet&) const;

	/** Объединенная операция пересечения и присваивания */
	BitsArraySet& operator &= (const BitsArraySet&);

	/** Объединенная операция объединения и присваивания */
	BitsArraySet& operator |= (const BitsArraySet&);

	/** Дополнение до универсума */
	BitsArraySet operator ~ () const;

	/** Вывод множества */
	void show();

	/** Конструктор множества */
	BitsArraySet(char, bool isRandomlyFilled = true);

	/** Базовый конструктор множества */
	BitsArraySet();
};

#endif //SETS_2_BITSARRAYSET_H
