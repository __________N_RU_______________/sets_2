#ifndef SETS_2_REPORTER_H
#define SETS_2_REPORTER_H

#include <iostream>
#include <string>

using namespace std;

template<typename Set> class Reporter {
private:
	/** Название отчета */
	std::string title;

	/** Время выполнения вычислений */
	float executionTime = 0.0;

	/** Количество повторений теста */
	int repeatCount = 0;
public:
	/** Выводит отчет программы на экран */
	inline Reporter report() {
		cout << endl;
		cout << "Название: '" << title << "'" << endl;
		cout << "Время вычисления: " << executionTime << " секунд" << endl;
		cout << "Количество повторений: " << repeatCount << endl;
		cout << "Количество тиков часов за секунду: " << CLOCKS_PER_SEC << endl << endl;

		return *this;
	};

	/** Выводит множества на экран */
	inline Reporter printSet(Set *set) {
		set->show();
		return *this;
	};

	/** Выводит результат на экран */
	inline Reporter printResult(Set *resultSet) {
		std::cout << std::endl << "Результат вычислений: " << std::endl;
		resultSet->show();
		return *this;
	};

	/** Задает название отчета */
	inline Reporter setTitle(const std::string &name) {
		title = name;
		return *this;
	};

	/** Задает время выполнения вычислений */
	inline Reporter setExecutionTime(float num) {
		executionTime = num;
		return *this;
	};

	/** Задает количество повторений теста */
	inline Reporter setRepeatCount(int count) {
		repeatCount = count;
		return *this;
	};
};

#endif //SETS_2_REPORTER_H
