#include <iostream>
#include <string>
#include "Reporter/Reporter.h"
#include "Profiler/Profiler.h"

#include "ArraySet/ArraySet.h"
#include "BitsArraySet/BitsArraySet.h"
#include "ListSet/ListSet.h"
#include "WordSet/WordSet.h"

using namespace std;

const long TEST_REPEAT_COUNT = 10000000;

int main() {
	srand(time(nullptr));
	setlocale(LC_ALL, "ru");

	ListSet A('A'), B('B'), C('C'), D('D'), E('E', false);
	auto title = ListSet::TEST_NAME;

	auto profiler = new Profiler;
	auto reporter = new Reporter<ListSet>;

	profiler->startProfile();

	for (int i = 0; i < 1; i++) {
		E = (A & B) | C | D;
	}

	profiler->stopProfile();

	reporter->setTitle(title)
		.setExecutionTime(profiler->getExecutionTime())
		.setRepeatCount(TEST_REPEAT_COUNT)
		.report()
		.printSet(&A)
		.printSet(&B)
		.printSet(&C)
		.printSet(&D)
		.printResult(&E);

	return 0;
}
